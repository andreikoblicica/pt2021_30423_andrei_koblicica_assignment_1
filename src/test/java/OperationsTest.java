import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OperationsTest {
    PolynomialMaker polynomialMaker=new PolynomialMaker();
    Operations operations=new Operations();

    @Test
    void shouldDoCorrectOperations(){   //a lot of tests in the same method
        Polynomial polynomial1=new Polynomial(polynomialMaker.formPolynomial("X+1")); //first set of polynomials
        Polynomial polynomial2=new Polynomial(polynomialMaker.formPolynomial("X-1"));

        Polynomial polynomial3=new Polynomial(polynomialMaker.formPolynomial("2*X^3+2*X-1")); //second set of polynomials
        Polynomial polynomial4=new Polynomial(polynomialMaker.formPolynomial("-X^2-2*X+4"));

        assertAll(() -> assertEquals("2*X",operations.add(polynomial1,polynomial2).toString()), //using assertAll so that everything is tested regardless of failed tests
                ()->assertEquals("2",operations.subtract(polynomial1,polynomial2).toString()),
                ()->assertEquals("X^2-1",operations.multiply(polynomial1,polynomial2).toString()),
                ()->assertEquals("1",operations.divide(polynomial1,polynomial2,true).toString()),//division
                ()->assertEquals("2",operations.divide(polynomial1,polynomial2,false).toString()),//modulo
                ()->assertEquals("1",operations.derive(polynomial1).toString()),
                ()->assertEquals("1",operations.derive(polynomial2).toString()),
                ()->assertEquals("0.50*X^2+X",operations.integrate(polynomial1).toString()),
                ()->assertEquals("0.50*X^2-X",operations.integrate(polynomial2).toString()),

                ()->assertEquals("2*X^3-X^2+3",operations.add(polynomial3,polynomial4).toString()),
                ()->assertEquals("2*X^3+X^2+4*X-5",operations.subtract(polynomial3,polynomial4).toString()),
                ()->assertEquals("-2*X^5-4*X^4+6*X^3-3*X^2+10*X-4",operations.multiply(polynomial3,polynomial4).toString()),
                ()->assertEquals("-2*X+4",operations.divide(polynomial3,polynomial4,true).toString()),//division
                ()->assertEquals("18*X-17",operations.divide(polynomial3,polynomial4,false).toString()),//modulo
                ()->assertEquals("6*X^2+2",operations.derive(polynomial3).toString()),
                ()->assertEquals("-2*X-2",operations.derive(polynomial4).toString()),
                ()->assertEquals("0.50*X^4+X^2-X",operations.integrate(polynomial3).toString()),
                ()->assertEquals("-0.33*X^3-X^2+4*X",operations.integrate(polynomial4).toString()));
    }
}
