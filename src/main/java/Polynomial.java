import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Polynomial {
    private ArrayList<Monomial> monomialList=new ArrayList<>();

    public Polynomial(){//constructor to create an empty polynomial and add monomials to it later
        // wow, such empty
    }
    public Polynomial(Polynomial polynomial){//to copy a polynomial into another, used for not changing the input polynomials during operations
        for(Monomial monomial: polynomial.getMonomialList()){
           monomialList.add(new Monomial(monomial.getCoefficient(), monomial.getPower()));
       }
    }
    public Polynomial(ArrayList<Monomial> monomialList){
        this.monomialList=monomialList;
        normalizeTerms();
    }
    public ArrayList<Monomial> getMonomialList() {
        return monomialList;
    }

    @Override
    public String toString(){ // this method transforms the monomial list of the polynomial into a string that will be displayed in the user interface
       if(monomialList.size()==0){
           return "0";}
       StringBuilder str=new StringBuilder("");
       for(Monomial monomial: monomialList){
            if(monomial.getCoefficient()>0 && monomialList.indexOf(monomial)!=0){
                str.append("+");}
            if(monomial.getCoefficient()!=1 && monomial.getCoefficient()!=-1){
                if(monomial.getCoefficient()==(int)monomial.getCoefficient()){ //if integer, dont want to print decimal places
                    str.append((int)monomial.getCoefficient());
                }
                else{
                    str.append(String.format("%.2f",monomial.getCoefficient()));}
                if(monomial.getPower()>0){
                    str.append("*");}
            }
            else if(monomial.getCoefficient()==-1){ //edge cases, dont want to print 1*X, -1*X, X^0, X^1
                str.append("-");
                if(monomial.getPower()==0){//if constant, need to print the 1 after the - sign
                    str.append("1");}
            }
            else if(monomial.getCoefficient()==1&&monomial.getPower()==0){ //same here, need to print 1
                str.append("1");}
            if(monomial.getPower()>0){
                str.append("X");
                if(monomial.getPower()>1){
                    str.append("^"+(int)monomial.getPower());}
            }
       }
       return str.toString();
    }

    public void normalizeTerms(){   //used for bringing the monomial list to a desired form
        sortTerms();
        mergeTerms();
        if(monomialList.size()==0){
            monomialList.add(new Monomial(0,0));
        }
    }
    private void sortTerms(){   //sorts the list of monomials decreasingly based on degree
        Collections.sort(monomialList, new Comparator<Monomial>(){
            public int compare(Monomial monomial1, Monomial monomial2){
                if(monomial1.getPower() == monomial2.getPower())
                    return 0;
                return monomial1.getPower() > monomial2.getPower() ? -1 : 1;
            }
        });
    }
    private void mergeTerms(){  //merges terms that have the same degree, keeping at most one for each value
        if(monomialList.size()>0){
            Polynomial auxPolynomial=new Polynomial();
            boolean isEmpty=true; int currentPosition=0;
            for(Monomial monomial: monomialList){
                if(isEmpty){
                    auxPolynomial.getMonomialList().add(monomial);
                    isEmpty=false;
                }
                else{ //all terms with the same power are adjacent because the list is sorted
                    if(monomial.getPower()==auxPolynomial.getMonomialList().get(currentPosition).getPower()){ //if the next one has the same degree as the previous one added to the new list, add it to it
                        if(auxPolynomial.getMonomialList().get(currentPosition).getCoefficient()==-monomial.getCoefficient()){ //the two might simplify
                            auxPolynomial.getMonomialList().remove(currentPosition); //gotta delete it then
                            if(currentPosition>0) { //dont want currentPosition to become negative, have to check
                                currentPosition--;
                            }
                            else{
                                isEmpty=true;
                            }
                        }else{
                            auxPolynomial.getMonomialList().get(currentPosition).setCoefficient(auxPolynomial.getMonomialList().get(currentPosition).getCoefficient()+ monomial.getCoefficient()); //add the coefficients
                        }
                    }
                    else{
                        currentPosition++;
                        auxPolynomial.getMonomialList().add(monomial); //if it doesnt match, just add it by itself
                    }
                }
            }
            monomialList=auxPolynomial.getMonomialList();
        }
    }
    public void reverseSigns(){
        for(Monomial monomial: monomialList){
            monomial.setCoefficient(-monomial.getCoefficient());
        }
    }

}
