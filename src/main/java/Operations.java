public class Operations {
    public Polynomial add(Polynomial polynomial1, Polynomial polynomial2){
        Polynomial result=new Polynomial();
        int i=0,j=0; //polynomial is sorted decreasingly
        while(i<polynomial1.getMonomialList().size()&&j<polynomial2.getMonomialList().size()){ //classic merge 2 sorted arrays algorithm
            Monomial monomial1=polynomial1.getMonomialList().get(i);
            Monomial monomial2=polynomial2.getMonomialList().get(j);
            if(monomial1.getPower()>monomial2.getPower()){ //add the largest to the new array
                result.getMonomialList().add(monomial1);
                i++;
            }
            else{
                if(monomial1.getPower()<monomial2.getPower()){ //add the largest to the new array
                    result.getMonomialList().add(monomial2);
                }
                else{ //if equal powers, need to add their coefficients
                    if(monomial1.getCoefficient()!=-1*monomial2.getCoefficient()) { //check if they dont simplify
                        result.getMonomialList().add(new Monomial(monomial1.getCoefficient() + monomial2.getCoefficient(), monomial1.getPower()));
                    } //if they do, simply add none of them
                    i++;
                }
                j++; //j gets incremented on both the if and else branch
            }
        }
        while(i<polynomial1.getMonomialList().size()){ //if terms left after the second polynomial finishes, add all of them
            result.getMonomialList().add(polynomial1.getMonomialList().get(i));
            i++;
        }
        while(j<polynomial2.getMonomialList().size()){//if terms left after the first polynomial finishes, add all of them
            result.getMonomialList().add(polynomial2.getMonomialList().get(j));
            j++;
        }
        return result;
    }

    public Polynomial subtract(Polynomial polynomial1, Polynomial polynomial2){
        Polynomial newPolynomial=new Polynomial(polynomial2);
        newPolynomial.reverseSigns();
        Polynomial result=add(polynomial1,newPolynomial); //subtraction=addition with reversed sign, just use add method
        return result;
    }

    public Polynomial multiply(Polynomial polynomial1, Polynomial polynomial2){
        Polynomial result=new Polynomial();
        for(Monomial monomial1: polynomial1.getMonomialList()){ //multiply each monomial from the first with each from the second
            for(Monomial monomial2: polynomial2.getMonomialList()){
                result.getMonomialList().add(new Monomial(monomial1.getCoefficient()* monomial2.getCoefficient(),monomial1.getPower()+ monomial2.getPower()));
            }
        }
        result.normalizeTerms();//result of multiplying them all is a mess, need to sort the terms decreasingly then merge ones with equal degree
        return result;
    }
    public Polynomial divide(Polynomial polynomial1, Polynomial polynomial2, boolean isDivision){
        Polynomial quotient=new Polynomial();
        Polynomial dividend=new Polynomial(polynomial1);//dont want to modify initial polynomial

        while(dividend.getMonomialList().size()>0 && polynomial2.getMonomialList().size()>0){ //check if they have elements
            if(dividend.getMonomialList().get(0).getPower()>=polynomial2.getMonomialList().get(0).getPower()){//while division can still be done
                Monomial monomial1=dividend.getMonomialList().get(0);
                Monomial monomial2=polynomial2.getMonomialList().get(0);
                Monomial auxMonomial=new Monomial(monomial1.getCoefficient()/monomial2.getCoefficient(),monomial1.getPower()- monomial2.getPower()); //result of monomial division
                quotient.getMonomialList().add(auxMonomial);
                Polynomial auxPolynomial=new Polynomial();
                auxPolynomial.getMonomialList().add(auxMonomial); //to use the multiply method, consider aux monomial as a polynomial with 1 term
                dividend=subtract(dividend,multiply(auxPolynomial,polynomial2)); //subtract from the first polynomial the product
            }
            else break;

        }
        if(isDivision){
            return quotient;//if the user clicks division, the quotient of the division is returned
        }
        else return dividend; //if the user clicks Modulo, the remainder of the division is returned
    }
    public Polynomial derive(Polynomial polynomial){
        Polynomial result=new Polynomial();
        for(Monomial monomial: polynomial.getMonomialList()){
            if(monomial.getPower()!=0){ //terms of degree 0 are gone, dont add them to result
                result.getMonomialList().add(new Monomial(monomial.getCoefficient()* monomial.getPower(),monomial.getPower()-1));  //coefficient gets multiplied, power decremented by 1
            }
        }
       return result;

    }
    public Polynomial integrate(Polynomial polynomial){
        Polynomial result=new Polynomial();
        for(Monomial monomial: polynomial.getMonomialList()){
                result.getMonomialList().add(new Monomial(monomial.getCoefficient()/ (monomial.getPower()+1),monomial.getPower()+1)); //power incremented by 1, coefficient divided
        }
        return result;
    }
}
