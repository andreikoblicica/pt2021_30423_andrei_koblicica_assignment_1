import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class GUIController {

    private Operations operations=new Operations();
    private PolynomialMaker polynomialMaker=new PolynomialMaker();
    private String polynomialString1="";    //these strings will always contain what is inside their corresponding text field
    private String polynomialString2="";
    private int currentPolynomial=1; //1 if the first text field is selected and editable by buttons, 2 if the second one is

    @FXML
    private TextField polynomialText1;
    @FXML
    private TextField polynomialText2;
    @FXML
    private TextField resultText;

    @FXML
    private Label currentPolynomialLabel;

    private void addCharacter(String character){
        if(currentPolynomial==1){ //checks in which text field to add the typed character
            polynomialString1+=character;
            polynomialText1.setText(polynomialString1); //updates the text field
        }
        else { //same here
            polynomialString2+=character;
            polynomialText2.setText(polynomialString2);
        }
    }
    @FXML
    private void pressKey(ActionEvent event){
        Button button= (Button) event.getSource();
        addCharacter(button.getText());
    }

    @FXML
    private void pressKeyDelete(ActionEvent event){
        if(currentPolynomial==1&&polynomialString1.length()>0){ //again, checks which field is selected, also checks if there is something to delete
            polynomialString1=polynomialString1.substring(0,polynomialString1.length()-1);
            polynomialText1.setText(polynomialString1);
        }
        else if(currentPolynomial==2&&polynomialString2.length()>0){
            polynomialString2=polynomialString2.substring(0,polynomialString2.length()-1);
            polynomialText2.setText(polynomialString2);
        }
    }
    @FXML
    private void pressAdd(ActionEvent event)throws Exception{
        if(polynomialMaker.validatePolynomial(polynomialString1)&&polynomialMaker.validatePolynomial(polynomialString2)){   //when the button is pressed, the strings are fetched and validated
            Polynomial polynomial1=new Polynomial(polynomialMaker.formPolynomial(polynomialString1));//if validation is ok for both, build the polynomials
            Polynomial polynomial2=new Polynomial(polynomialMaker.formPolynomial(polynomialString2));
            resultText.setText("P1 + P2 = "+operations.add(polynomial1,polynomial2).toString()); //display the result of the addition
        }
        else{
            loadAlertBox("Make sure you insert correct polynomials!"); //if validation returns false, tell the user to reconsider his life choices
        }
    }
    @FXML
    private void pressSubtract(ActionEvent event)throws Exception{  //same comments as add
        if(polynomialMaker.validatePolynomial(polynomialString1)&&polynomialMaker.validatePolynomial(polynomialString2)){
            Polynomial polynomial1=new Polynomial(polynomialMaker.formPolynomial(polynomialString1));
            Polynomial polynomial2=new Polynomial(polynomialMaker.formPolynomial(polynomialString2));
            resultText.setText("P1 - P2 = "+operations.subtract(polynomial1,polynomial2).toString());
        }
        else{
            loadAlertBox("Make sure you insert correct polynomials!");
        }
    }
    @FXML
    private void pressMultiply(ActionEvent event)throws Exception{ //same comments as add
        if(polynomialMaker.validatePolynomial(polynomialString1)&&polynomialMaker.validatePolynomial(polynomialString2)){
            Polynomial polynomial1=new Polynomial(polynomialMaker.formPolynomial(polynomialString1));
            Polynomial polynomial2=new Polynomial(polynomialMaker.formPolynomial(polynomialString2));
            resultText.setText("P1 * P2 = "+operations.multiply(polynomial1,polynomial2).toString());
        }
        else{
            loadAlertBox("Make sure you insert correct polynomials!");
        }
    }
    @FXML
    private void pressDivide(ActionEvent event)throws Exception{ //same comments as add
        if(!polynomialString2.equals("0")){ //need to check extra condition, 0 is a valid polynomial but not suitable for division
            if(polynomialMaker.validatePolynomial(polynomialString1)&&polynomialMaker.validatePolynomial(polynomialString2)){
                Polynomial polynomial1=new Polynomial(polynomialMaker.formPolynomial(polynomialString1));
                Polynomial polynomial2=new Polynomial(polynomialMaker.formPolynomial(polynomialString2));
                resultText.setText("P1 / P2 = "+operations.divide(polynomial1,polynomial2,true).toString());
            }
            else{
                loadAlertBox("Make sure you insert correct polynomials!");
            }
        }
        else{
            loadAlertBox("You cannot divide by 0!"); //duuh
        }
    }
    @FXML
    private void pressModulo(ActionEvent event)throws Exception{ //same comments as divide
        if(!polynomialString2.equals("0")){
            if(polynomialMaker.validatePolynomial(polynomialString1)&&polynomialMaker.validatePolynomial(polynomialString2)){
                Polynomial polynomial1=new Polynomial(polynomialMaker.formPolynomial(polynomialString1));
                Polynomial polynomial2=new Polynomial(polynomialMaker.formPolynomial(polynomialString2));
                resultText.setText("P1 % P2 = "+operations.divide(polynomial1,polynomial2,false).toString());
            }
            else{
                loadAlertBox("Make sure you insert correct polynomials!");
            }
        }
        else{
            loadAlertBox("You cannot divide by 0!");
        }
    }
    @FXML
    private void pressDerive(ActionEvent event)throws Exception{
        String polynomialString;
        String text;
        if(currentPolynomial==1){   //checks if result will be P1' or P2'
            polynomialString=polynomialString1;
            text="P1' = ";
        }else {
            polynomialString=polynomialString2;
            text="P2' = ";
        }
            if(polynomialMaker.validatePolynomial(polynomialString)){   //validates the string
                Polynomial polynomial=new Polynomial(polynomialMaker.formPolynomial(polynomialString));
                resultText.setText(text+operations.derive(polynomial).toString()); //updates text field with result
            }
            else{
                loadAlertBox("Make sure you insert correct polynomials!");
            }
    }
    @FXML
    private void pressIntegrate(ActionEvent event)throws Exception{ //same comments as division
        String polynomialString;
        String text;
        if(currentPolynomial==1){
            polynomialString=polynomialString1;
            text="∫P1 = ";

        }else {
            polynomialString=polynomialString2;
            text="∫P2 = ";
        }
        if(polynomialString.equals("0")){
            resultText.setText(text+"C"); //also need to add a C for indefinite integral
        }
        else{
            if(polynomialMaker.validatePolynomial(polynomialString)){
                Polynomial polynomial=new Polynomial(polynomialMaker.formPolynomial(polynomialString));
                resultText.setText(text+operations.integrate(polynomial).toString()+"+C");
            }
            else{
                loadAlertBox("Make sure you insert correct polynomials!");
            }
        }

    }
    @FXML
    private void pressClear(ActionEvent event){ //wipes everything clean
        polynomialString1="";
        polynomialString2="";
        polynomialText1.clear();
        polynomialText2.clear();
        resultText.clear();
    }
    @FXML
    private void pressP1TextField(MouseEvent event){ //if the user clicks the first text field, currentPolynomial variable and label switch
        currentPolynomial=1;
        currentPolynomialLabel.setText("Edit polynomial P1:");
    }
    @FXML
    private void pressP2TextField(MouseEvent event){ //same as above
        currentPolynomial=2;
        currentPolynomialLabel.setText("Edit polynomial P2:");
    }

    public void loadAlertBox(String message)throws Exception{
        Stage stage=new Stage(); //need a new window
        stage.setTitle("Error");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL); //can only do something else if this is taken care of

        FXMLLoader loader=new FXMLLoader(getClass().getResource("AlertBox.fxml"));
        Parent root = loader.load();

        AlertBoxController alertBoxController=loader.getController();
        alertBoxController.initializeLabel(message); //pass error message to be displayed

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
    }
}
