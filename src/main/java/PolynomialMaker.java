
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolynomialMaker {
    String monomialPattern="-?[1-9][0-9]{0,3}\\*X\\^[0-9]{1,4}"; //pattern that each monomial string has to match

    private String normalizeMonomialString(String string){ //treating some edge cases so that monomials can be properly validated by the regex expression
        StringBuilder result=new StringBuilder();
        if(string.length()>0){
            if(string.indexOf('X')==-1){ //if monomial contains no X, might be a constant
                result.append(string+"*X^0");
                return result.toString();
            }
            if(string.equals("X")){
                result.append("1*"+string+"^1");
                return result.toString();
            }
            if(string.equals("-X")){
                result.append("-1*"+string.substring(1)+"^1");
                return result.toString();
            }

            if(string.charAt(string.length()-1)=='X'){
                result.append(string+"^1");
                return result.toString();
            }
            if(string.charAt(0)=='X'){
                result.append("1*"+string);
                return result.toString();
            }
            if(string.charAt(0)=='-'&&string.charAt(1)=='X'){
                result.append("-1*"+string.substring(1));
                return result.toString();
            }
        }
        return string;
    }

    private String normalizePolynomialString(String string){
        StringBuilder result=new StringBuilder();
        for(int i=0;i<string.length();i++){
            if(string.charAt(i)=='-'&&i!=0){
                result.append("+-");    //adding + before every - so that the string can be split by + and the sign preserved
            }
            else{
                result.append(string.charAt(i));
            }
        }
        return result.toString();
    }

    private ArrayList<String> formMonomialStrings(String string){   //every monomial is in a separate string
        ArrayList<String> monomialStrings = new ArrayList<>();
        String newString = normalizePolynomialString(string);//adds + signs before minuses in order to split by +
        String[] monomials = newString.split("\\+");

        for (int i = 0; i < monomials.length; i++) {
            monomialStrings.add(normalizeMonomialString(monomials[i])); //dont wanna work with array, making it arrayList
        }
        return monomialStrings;
    }
    public ArrayList<Monomial> formPolynomial(String string){
        ArrayList<Monomial> monomialList=new ArrayList<>();
        if(string.equals("0")){ //special case to treat separately
            monomialList.add(new Monomial(0,0));
        }
        else{
            ArrayList<String> monomialStrings=formMonomialStrings(string);
            for (String monomialString : monomialStrings) {
                int coefficient, power;
                String[] parts = monomialString.split("\\*");   //first split by *
                coefficient = Integer.parseInt(parts[0]);   //first part is the coefficient
                String[] parts2 = parts[1].split("\\^"); //split second part by ^ then
                power = Integer.parseInt(parts2[1]);   //second part of that is the power
                monomialList.add(new Monomial(coefficient, power));
            }
        }
        return monomialList;
    }
    public boolean validatePolynomial(String string){
        if(string.equals("0")){ //correct polynomial, special case
            return true;
        }
        ArrayList<String> monomialStrings=formMonomialStrings(string);
        Pattern pattern=Pattern.compile(monomialPattern);
        for(String monomialString: monomialStrings){    //checking for each monomial string
            Matcher matcher=pattern.matcher(monomialString);
            if(!matcher.matches()){ //if any of the monomials dont match, validation ends
                return false;
            }
        }
        return true;    //all good
    }
}
